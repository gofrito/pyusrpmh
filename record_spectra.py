#!/usr/bin/env python
"""
Created on Fri Oct 10 12:29:10 2014

    record_spectra.py
    most of the tools were taken from spectra_humain.py
    @author: molera, kallunki
    """

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from optparse import OptionParser
import math, sys, time
from datetime import datetime, timedelta
import struct

########################
class tune(gr.feval_dd):
    """
    This class allows C++ code to callback into python.
    """
    def __init__(self, tb):
        gr.feval_dd.__init__(self)
        self.tb = tb    

    def eval(self, ignore):
        """
        This method is called from blocks.bin_statistics_f when it wants
        to change the center frequency.  This method tunes the front
        end to the new center frequency, and returns the new frequency
        as its result.
        """
        try:
            new_freq = self.tb.set_next_freq()
            # wait until msgq is empty before continuing
            while(self.tb.msgq.full_p()):
                time.sleep(0.1)
            return new_freq
        except Exception as err:
            print("tune: Exception: ", err)

#########################
class parse_msg(object):
    def __init__(self, msg):
        self.center_freq = msg.arg1()
        self.vlen = int(msg.arg2())
        assert(msg.length() == self.vlen * gr.sizeof_float)
        t = msg.to_string()
        self.raw_data = t
        self.data = struct.unpack('%df' % (self.vlen,), t)

#########################
class spectrometer(gr.top_block):
    def __init__(self, address="addr=10.7.6.18", fft_size=256, 
                channel_bandwidth=97656.25 , samp_rate=20e6, 
                min_freq=275e6, max_freq=1475e6, lo_offset=13e6,
                tune_delay=0.0015625, dwell_delay=0.0015625, gain=10):
        gr.top_block.__init__(self, "Spectrometer")
        ##################################################
        # Parameters
        ##################################################
        self.address = address
        self.fft_size = fft_size
        self.channel_bandwidth = channel_bandwidth
        self.samp_rate = samp_rate
        self.min_freq = min_freq
        self.max_freq = max_freq
        self.lo_offset = lo_offset
        self.tune_delay = tune_delay
        self.dwell_delay = dwell_delay
        self.gain = gain

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0 = uhd.usrp_source(
        	device_addr=address,
        	stream_args=uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )

        self.uhd_usrp_source_0.set_time_now(uhd.time_spec(time.time()),uhd.ALL_MBOARDS)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.usrp_rate = usrp_rate = self.uhd_usrp_source_0.get_samp_rate()
        self.uhd_usrp_source_0.set_gain(gain, 0)
        self.uhd_usrp_source_0.set_antenna("RX2", 0)
        self.uhd_usrp_source_0.set_bandwidth(samp_rate, 0)
        self.uhd_usrp_source_0.set_clock_source("external", 0)
        self.uhd_usrp_source_0.set_time_source("external", 0)
        #
        self.fft_vxx_0 = fft.fft_vcc(fft_size, True,(window.blackmanharris(fft_size)), True, 1)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, fft_size)
        self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(fft_size)

        ###From usrp_spectrum_sense
        #self.freq_step = self.nearest_freq((0.75*self.usrp_rate),
        #                 self.channel_bandwidth)

        self.freq_step = self.nearest_freq((0.6*self.usrp_rate),self.channel_bandwidth)
        self.min_center_freq = self.min_freq + (self.freq_step/2)
        nsteps = math.ceil((self.max_freq - self.min_freq) / self.freq_step)
        self.max_center_freq = self.min_center_freq + (nsteps * self.freq_step)
        self.next_freq = self.min_center_freq
        #
        tune_delay  = max(0, int(round(self.tune_delay * usrp_rate / 
                      self.fft_size)))  # in fft_frames
        dwell_delay = max(1, int(round(self.dwell_delay * usrp_rate / 
                      self.fft_size))) # in fft_frames

        #
        self.msgq = gr.msg_queue(16)
        self._tune_callback = tune(self)
        self.stats = blocks.bin_statistics_f(self.fft_size, self.msgq,
                                        self._tune_callback, tune_delay,
                                        dwell_delay)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.uhd_usrp_source_0, 0), 
                    (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), 
                    (self.fft_vxx_0, 0))
        self.connect((self.fft_vxx_0, 0), 
                    (self.blocks_complex_to_mag_squared_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0, 0) , 
                    (self.stats, 0))
    
    def set_next_freq(self):
        target_freq = self.next_freq
        self.next_freq = self.next_freq + self.freq_step
        if self.next_freq >= self.max_center_freq:
            self.next_freq = self.min_center_freq
        if not self.set_freq(target_freq):
            print("Failed to set frequency to", target_freq)
            sys.exit(1)
        return target_freq                

    def set_freq(self, target_freq):
        r = self.uhd_usrp_source_0.set_center_freq(
            uhd.tune_request(target_freq, 
            rf_freq=(target_freq + self.lo_offset), 
            rf_freq_policy=uhd.tune_request.POLICY_MANUAL))
        if r:
           return True
        return False        

    def nearest_freq(self, freq, channel_bandwidth):
        freq = round(freq/channel_bandwidth, 0) * channel_bandwidth
        return freq

#########################
def main_loop(tb, end_time, output_dir):
    def bin_freq(i_bin, center_freq):
        freq = center_freq - (tb.usrp_rate/2) + (tb.channel_bandwidth*i_bin)
        return freq

    bin_start = int(tb.fft_size * ((1-0.6)/2))
    bin_stop = int(tb.fft_size - bin_start)
    dfstart = datetime.utcnow()
    ofile= 'raw_' + dfstart.strftime("%Y%m%d_%H%M%S") + '.dat'
    f1=open(output_dir + ofile, 'wb', 8528)
    cur_time = datetime.utcnow()
    while cur_time < end_time :
        # Get the next message sent from the C++ code (blocking call).
        # It contains the center frequency and the mag squared of the fft
        m = parse_msg(tb.msgq.delete_head())
        cur_time = datetime.utcnow()
        center_freq = m.center_freq
        start_freq = bin_freq(bin_start, center_freq)
        stop_freq = bin_freq(bin_stop, center_freq)
        if ((cur_time >= dfstart + timedelta(minutes=2)) and 
           (center_freq == tb.min_center_freq)):
            f1.close()
            dfstart = cur_time
            ofile= 'raw_' + dfstart.strftime("%Y%m%d_%H%M%S") + '.dat'
            f1=open(output_dir + ofile, 'wb', 8528)
        f1.write(struct.pack('H', cur_time.utcnow().year))
        f1.write(struct.pack('H', cur_time.utcnow().month))
        f1.write(struct.pack('H', cur_time.utcnow().day))
        f1.write(struct.pack('H', cur_time.utcnow().hour))
        f1.write(struct.pack('H', cur_time.utcnow().minute))
        f1.write(struct.pack('H', cur_time.utcnow().second))
        f1.write(struct.pack('i', cur_time.utcnow().microsecond))
        f1.write(struct.pack('H', tb.fft_size))
        f1.write(struct.pack('f', tb.usrp_rate))
        f1.write(struct.pack('f', tb.channel_bandwidth))
        f1.write(struct.pack('f', m.center_freq))
        f1.write(struct.pack('f', start_freq))
        f1.write(struct.pack('H', bin_start))
        f1.write(struct.pack('f', stop_freq))
        f1.write(struct.pack('H', bin_stop))
        f1.write(m.raw_data)
    f1.close()

#########################
if __name__ == '__main__':
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    parser.add_option("-a", "--address", dest="address", type="string", 
                     default="addr=10.7.6.18",
                     help="Set IP address [default=%default]")
    parser.add_option("", "--fft-size", dest="fft_size", type="intx", 
                     default=256, help="Set Size of fft [default=%default]")
    parser.add_option("", "--channel-bandwidth", dest="channel_bandwidth", 
                     type="eng_float", 
                     default=eng_notation.num_to_str(97656.25),
                     help="Bandwidth of fft bins [default=%default]")
    parser.add_option("", "--samp-rate", dest="samp_rate", type="eng_float", 
                     default=eng_notation.num_to_str(20e6),
                     help="Set Sample Rate [default=%default]")
    parser.add_option("", "--min-freq", dest="min_freq", type="eng_float", 
                     default=eng_notation.num_to_str(275e6),
                     help="Set center_freq_min [default=%default]")
    parser.add_option("", "--max-freq", dest="max_freq", type="eng_float", 
                     default=eng_notation.num_to_str(1275e6),
                     help="Set center_freq_max [default=%default]")
    parser.add_option("", "--lo-offset", dest="lo_offset", type="eng_float", 
                     default=eng_notation.num_to_str(13e6),
                     help="Set the lo_offset [default=%default]")
    parser.add_option("", "--tune-delay", dest="tune_delay", type="eng_float", 
                     default=0.0015625,
                     help="delay (s) after changing freq. [default=%default]")
    parser.add_option("", "--dwell-delay", dest="dwell_delay", 
                     type="eng_float", default=0.0015625,
                     help="dwell time (s) at a given freq. [default=%default]")
    parser.add_option("-g", "--gain", dest="gain", type="eng_float", 
                     default=eng_notation.num_to_str(10),
                     help="Set Default Gain [default=%default]")
    (options, args) = parser.parse_args()
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print("Error: failed to enable realtime scheduling.")
    cur_time = datetime.utcnow()
    #end_time = cur_time.replace(hour=16, minute=30, second=0, microsecond=0)
    end_time = cur_time.replace(minute=cur_time.minute+5)
    data_dir = '/local/kallunki/DATA/CUR/'

    tb = spectrometer(address=options.address, fft_size=options.fft_size, 
                     channel_bandwidth=options.channel_bandwidth, 
                     samp_rate=options.samp_rate, min_freq=options.min_freq, 
                     max_freq=options.max_freq, lo_offset=options.lo_offset, 
                     tune_delay=options.tune_delay, 
                     dwell_delay=options.dwell_delay, gain=options.gain)

    tb.start()

    if datetime.utcnow() < end_time:
        try:
            main_loop(tb, end_time, data_dir)
        except KeyboardInterrupt:
            pass
    tb.stop()
    tb.wait()