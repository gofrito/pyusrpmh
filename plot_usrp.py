#!/usr/bin/env python
"""
Created on Wed Mar 18 15:28:19 2015
    plot_usrp.py 
    plot the spectrum recorded with the USRP device. Settings and data format
    are adjusted to the recording mode. See spectra_usrp.py
    @author: molera, kallunki
    """

import numpy as np
import matplotlib.pyplot as plt
import argparse

class plot_usrp:
    def __init__(self):
        self.data = []
        self.freq = []
        plot_help = ' 0- Plot the average spectra with respect to the time \n 1- Plot the spectrogram of a series of spectra'
        parser = argparse.ArgumentParser()
        parser.add_argument('filename', nargs='?', help='USRP binary data file', default='/Users/molera/Downloads/raw_20150318_112459.dat')
        parser.add_argument('-p', '--plot', type=int, default=0, help =plot_help)
        args = parser.parse_args()

        self.filename = args.filename
        self.plot     = args.plot


    def read_raw_usrp(self):
        """This function reads RAW files from USRP spectrometer"""
        dum = np.fromfile(self.filename, dtype=("H2,H2,H2,H2,H2,H2,i4,H2,f4,f4,f4,f4,H2,f4,H2,(256)f4"))
        center_freq = dum['f10']
        freq        = dum['f10'] - dum['f8']/2 + dum['f9']*np.arange(np.unique(dum['f12'])[0], np.unique(dum['f14'])[0])[:, None]
        data        = 10. * np.log10(dum['f15'][:, np.unique(dum['f12'])[0]:np.unique(dum['f14'])[0]]/np.unique(dum['f8'])[0])
            
        nband   = len(np.unique(center_freq))
        nrecord = (data.size)/(nband*freq.shape[0])
        
        self.data = (np.reshape(data[0:nrecord*nband,0:data.shape[1]], (nrecord, nband*freq.shape[0]))).transpose()
        self.freq = np.unique(np.reshape(freq[0:data.shape[1],0:nrecord*nband], (nrecord, nband*freq.shape[0])))    
        print(nband)
        print(nrecord)
        print(np.shape(self.data))
        
        self.year      = dum['f0'][::nband]
        self.month     = dum['f1'][::nband]
        self.day       = dum['f2'][::nband]
        self.hour      = dum['f3'][::nband]
        self.minute    = dum['f4'][::nband]
        self.second    = dum['f5'][::nband]
        self.msecond   = dum['f6'][::nband]
    
        self.n_fft     = dum['f7'][0]
        self.usrp_rate = dum['f8'][0]
        self.chanel_bw = np.unique(dum['f9'])[0]

    def plot_data(self):
        if self.plot == 0:
            print('Ploting the power spectra vs. the Frequency')
            plt.figure()
            plt.plot(np.divide(self.freq,1e6), self.data)
            plt.title('Power spectra')
            plt.xlabel('Frequency [GHz]')
            plt.ylabel('Power (dB)')
            plt.show()
        elif self.plot == 1:
            print('Printing the spectrogram')
            plt.figure()
            plt.imshow(self.data, aspect='auto')
            plt.colorbar()
            plt.title('Spectrogram')
            plt.xlabel('Time/Samples')
            plt.ylabel('Frequency [Hz]')
            plt.show()

usrp = plot_usrp()
usrp.read_raw_usrp()
usrp.plot_data()

