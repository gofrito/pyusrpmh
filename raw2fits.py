#!/usr/bin/env python
"""
Created on Fri Oct 10 12:29:10 2014

    raw2fits.py
    This program reads raw spectra produced by the USRP and turns them into FITS file
    File extracted from raw2fits - marque, ROB
    @author: molera, kallunki
    """

import os
import shutil
import fnmatch
import numpy as np
import astropy.io.fits as pf
import datetime
from time import gmtime, strftime

class Raw2Fits():
#
      def __init__(self, station=None, date=None):
          if date == None:
             self.date = datetime.datetime.today()
          else:
             self.date = datetime.datetime.strptime(date, "%Y%m%d")
          self.cur_dir = '/local/kallunki/DATA/CUR/'
          self.raw_dir = '/local/kallunki/DATA/RAW/' + self.date.strftime("%Y/%m/%d/")
          self.log_dir = '/local/kallunki/DATA/LOG/'
          self.fits_dir = '/local/kallunki/DATA/FITS/' + self.date.strftime("%Y/%m/%d/")
          self.station = station
          #create fits directory
          self.create_fits_dir()
          self.create_raw_dir()
          #Create list of files to process and sort by name
          try:
             self.cur_dir_content = os.listdir(self.cur_dir)
          except OSError:
             self.cur_dir_content = []
          self.cur_dir_content.sort()
	      #Create the list of files to process
          self.list_file = self.get_file_list()

      def create_fits_dir(self):
          try:
             os.makedirs(self.fits_dir)
          except OSError:
             pass

      def create_raw_dir(self):
          try:
             os.makedirs(self.raw_dir)
          except OSError:
             pass

      def write2log(self, buff):
          cur_time = datetime.datetime.utcnow()
          f=open(self.log_dir + 'LOG' + cur_time.strftime("%Y%m%d_%H%M%S") + '.txt', 'a+')
          f.write(buff + "\n")
          f.close()     

      def move_raw_to_arch(self, filename):
          """This function moves the processed RAW file to a temporary archive """
          dumf=filename[filename.find('.dat')-19:]
          shutil.move(self.cur_dir+dumf, self.raw_dir+dumf)
          self.write2log("File: " + dumf + " moved to: " + self.raw_dir)

      def get_file_list(self):
          """returns the list of files to process"""
          list_file=[]
          str_date = self.date.strftime("%Y%m%d")
          for ifile in self.cur_dir_content:
              if (fnmatch.fnmatch(ifile , 'raw_' + str_date + '_*.dat')):
                 dumt = (datetime.datetime.utcnow() -
                        datetime.datetime.utcfromtimestamp(
                        os.path.getmtime(self.cur_dir + ifile)))
                 if (dumt.total_seconds() > 180):#960
                    list_file.append(self.cur_dir + ifile)
                 else:
                    self.write2log("File: " + self.cur_dir + ifile + 
                        " skipped because it is still being written to")
          return list_file

      def read_raw_usrp(self,filename):
          """This function reads RAW files from USRP spectrometer"""
          dum = np.fromfile(filename, 
              dtype=("H2,H2,H2,H2,H2,H2,i4,H2,f4,f4,f4,f4,H2,f4,H2,(256)f4"))
          center_freq = dum['f10']          
          freq = dum['f10'] - dum['f8']/2 + dum['f9']*np.arange(
                 np.unique(dum['f12'])[0], np.unique(dum['f14'])[0])[:, None]
          data= 10. * np.log10(dum['f15'][:, np.unique(dum['f12'])[0]:np.unique(dum['f14'])[0]]/np.unique(dum['f8'])[0])#-10*np.log10(np.min(dum['f15'])/np.unique(dum['f8'])[0])
          nband = len(np.unique(center_freq))
          nrecord = (data.size)/(nband*freq.shape[0])
          self.data = (np.reshape(data, 
                      (nrecord, nband*freq.shape[0]))).transpose()
          self.frequency = np.unique(np.reshape(freq, 
                      (nrecord, nband*freq.shape[0])))
          self.year = dum['f0'][::nband]
          self.month = dum['f1'][::nband]
          self.day = dum['f2'][::nband]
          self.hour = dum['f3'][::nband]
          self.minute = dum['f4'][::nband]
          self.second = dum['f5'][::nband]
          self.microsecond = dum['f6'][::nband]
          self.channel_bandwidth = np.unique(dum['f9'])[0]
         
      def make_fits(self, calib= False):
          """This function creates fits files from raw measurements """
          yyyy = self.year[0]
          mm = self.month[0]
          dd = self.day[0]
          hh = self.hour[0]
          mn = self.minute[0]
          ss = self.second[0]
          time_arr = (self.hour*3600 + self.minute*60 + self.second + self.microsecond/1e6)
          cdelt1 = np.mean((time_arr - np.roll(time_arr, 1))[1:])
          level = 0
          strlev = "Data processing level: Uncalibrated"
          unit = 'dB'
          unit_lab = 'dB above Noise Floor'
          pol = 'STOKESI'
          pol_lab = 'Intensity'
          inst_code = 60
          out_filename = (self.station + '_' + '%04d'%yyyy + '%02d'%mm + 
                         '%02d'%dd + '_' + '%02d'%hh + '%02d'%mn + '%02d'%ss + 
                         'i_' + '%02d'%inst_code + '.fit')
          #Creates fits file
          phdu = pf.PrimaryHDU()
          cphdu = pf.CompImageHDU(np.ascontiguousarray(self.data))
          cphdu.header.set('DATE', strftime("%Y-%m-%dT%H:%M:%S", 
                            gmtime()), ' [UTC] date of fits file creation')
          cphdu.header.set('DATE-OBS', strftime("%Y-%m-%dT%H:%M:%S", 
                            (yyyy, mm, dd, hh, mn, ss, 0, 0, 0)), 
                            '[UTC] time of first record')
          cphdu.header.set('LEVEL', level, strlev)
          cphdu.header.set('ORIGIN', 'MRO', 'Metsahovi Radio Observatory')
          cphdu.header.set('TELESCOP', 'MRO-Log', 'telescope name')
          cphdu.header.set('INSTRUME', 'MRO-USPR', 'instrument name')
          cphdu.header.set('OBJECT', 'SUN RADIO', 'observed object')
          cphdu.header.set('POLAR', pol, pol_lab)
          cphdu.header.set('BUNIT', unit, unit_lab)
          cphdu.header.set('DATAMIN', self.data.min(), 'minimum value')
          cphdu.header.set('DATAMAX', self.data.max(), 'maximum value')
          #phdu.scale('int16',bzero=3)
          cphdu.header.set('CTYPE1', 'TIME', 'axis 1')
          cphdu.header.set('CTYPE2', 'FREQUENCY', 'axis 2')
          cphdu.header.set('CUNIT1', 'UTC', 'axis 1 unit')
          cphdu.header.set('CUNIT2', 'MHz', 'axis 2 unit')
          cphdu.header.set('CDELT1', cdelt1, 
                             '[s] step between points on axis 1 (average)')
          cphdu.header.set('CDELT2', self.channel_bandwidth/1e6, 
                             '[MHz] step between points on axis 2')
          cphdu.header.set('CRPIX1', 0, 'reference pixel on axis 1')
          cphdu.header.set('CRPIX2', 0, 'reference pixel on axis 2')
          cphdu.header.set('CRVAL1', time_arr[0], 
                             '[s] value of reference pixel on axis 1')
          cphdu.header.set('CRVAL2', self.frequency[0]/1e6, 
                             '[MHz] value of reference pixel on axis 2')
          cphdu.header.add_comment('HSRS-1 is based on a USRP N210 receiver.')
          cphdu.header.add_comment('Software development is made using Gnuradio')
          cphdu.header.add_comment('Contact person: J. Kallunki, juha.kallunki@aalto.fi')
          cphdu.header.add_comment('Metsahovi Radio Observatory')
          hdu = pf.HDUList(hdus=phdu)
          hdu.append(cphdu)
          sff = str(len(self.frequency)) + 'D'
          sft = str(len(time_arr)) + 'D'
          c1 = pf.Column(name='FREQUENCY', format=sff)
          c2 = pf.Column(name='TIME', format=sft)
          tbhdu = pf.new_table([c1, c2], nrows=1)
          tbhdu.data.field(0)[0][:] = self.frequency/1e6
          tbhdu.data.field(1)[0][:] = time_arr
          tbhdu.header.add_comment('[MHz] Frequency')
          tbhdu.header.add_comment('[s] Time since beginning of the day')
          hdu.append(tbhdu)
          hdu.writeto(self.fits_dir + out_filename, clobber=True)
          self.write2log("Fits file: " + out_filename + " has been written")

if __name__ == '__main__':
   r2f=Raw2Fits(station='MRO')
   for filename in r2f.list_file:
       r2f.read_raw_usrp(filename)
       r2f.make_fits(calib=False)
       r2f.move_raw_to_arch(filename)