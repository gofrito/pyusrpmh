1- Current Files in this project
    - raw2fits.py
    - record_spectra.py
    - plot_usrp.py
    - fits_inspector.py

2- Introduction
The current software allows us to record data from the USRP device and store into
the directory selected by the user (**record_spectra.py**)
**raw2fits.py** converts the raw data into FITS file. 
**plot_usrp.py** plots the temporal and spectral evolution of the raw data recorded.
**fits_inspector.py** concatenates available fits file and display long data sets.

3- Requirements